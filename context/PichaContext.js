import { useDisclosure } from "@chakra-ui/hooks"
import React, { createContext } from "react"

const PiContext = createContext()



export const PichaContext = (props) => {
    const [{ isOpen, onOpen, onClose }] = useDisclosure()
    return (
        <PiContext.Provider value={[isOpen, onOpen, onClose]}>
            {props.children}
        </PiContext.Provider>
    )
}