<h2>Projeto teste para a Petrec<h2>

<strong>Reconstrução do sistema web Picha</strong>

<h3>Iniciando a aplicação</h3>

Instalando as dependencias

```bash
npm i
# or
yarn
```

Iniciando aplicação localmente

```bash
npm run dev
# or
yarn dev
```

<p>O Sistema local</p>(http://localhost:3000)

<p>O Sitema  remoto esta hospedados na URL</p> https://petrec.vercel.app/
<h3>Funcionalidades</h3>

<p>A aplicação foi recriada com Base na aplicação Picha que consome dados de uma api pública de imagens aleatórias</p>
<p><strong>Tecnologia:</strong> React NextJS - Chakra UI</p>

<h3>Slide de imagens</h3>

<p>Inclusão do carrousel de imagens que consome dentro de um tempo pré definido as imagens da api pública e aplica o autoplay</p>
<p><strong>Tecnologia usadas :</strong> AliceCarousel</p>

<h3>Machine Learning</h3>

<p>Usando uma blibioteca de Machine learning enviamos a imagens para receber predicçoes das imagens com tres possibilidades.</p>
<p><strong>Tecnologia usada:</strong>ML5 e MobileNet</p>

<h3>Gráficos</h3>

<p>Foi implementado o gráfico de barras usando as datas que foram geradas as fotos.</p>
<p><strong>Tecnologia usada:</strong> D3<p>
