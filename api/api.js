import axios from "axios";


const api = axios.create({
    baseURL: "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1",
});

export default api;