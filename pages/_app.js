import { ChakraProvider } from "@chakra-ui/react"
import { PichaContext } from "../context/PichaContext"

function MyApp({ Component, pageProps }) {
  return (
    <ChakraProvider>
      <Component {...pageProps} />
    </ChakraProvider>

  )
}

export default MyApp
