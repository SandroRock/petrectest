import { Button } from "@chakra-ui/react"
import React, { useContext } from 'react'
import { PichaContext } from "../../context/PichaContext"
const PichaButton = (props) => {
    const context = useContext(PichaContext)
    
    return (
        <Button size={props.size} colorScheme={props.color} >{props.text}</Button>

    )
}
export default PichaButton