import { Button } from "@chakra-ui/button"
import { Box } from "@chakra-ui/layout"
import { Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader } from "@chakra-ui/modal"
import BarChart from "react-d3-components/lib/BarChart"

const PichaModal = (props) => {

    return (
        <Modal closeOnOverlayClick={props.closeOnOverlayClick} isOpen={props.isOpen} onClose={props.onClose} size="lg">

            <ModalContent w={600}>
                <ModalHeader>Data Record</ModalHeader>
                <ModalCloseButton />
                <ModalBody pb={6} p={5}>
                    <Box mt='150' mr="20">
                        <BarChart
                            data={props.data}
                            width={500}
                            height={200}
                            margin={{ top: 10, bottom: 50, left: 50, right: 10 }}
                        />
                    </Box>
                </ModalBody>
                <ModalFooter>
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}

export default PichaModal