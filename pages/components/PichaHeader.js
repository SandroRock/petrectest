import { Button } from "@chakra-ui/button"
import { FormControl, FormLabel } from "@chakra-ui/form-control"
import { useDisclosure } from "@chakra-ui/hooks"
import { Input } from "@chakra-ui/input"
import { Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay } from "@chakra-ui/modal"
import { Textarea } from "@chakra-ui/textarea"
import React from "react"
import styles from '../../styles/components/PichaHeader.module.css'
const PichaHeader = () => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    return (
        <>
            <div className={styles.header}>
                <h1>Picha!</h1>
                <Button size="lg" colorScheme="teal" onClick={onOpen}>GOT FEEDBACK</Button>
            </div>
            <Modal closeOnOverlayClick={false} isOpen={isOpen} onClose={onClose} >
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Feedback</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody pb={6} p={5}>
                        <FormControl id="email">
                            <FormLabel>Email address</FormLabel>
                            <Input type="text" />
                            <Textarea placeholder="Write your message" cols={10} mt={15} />
                        </FormControl>
                    </ModalBody>

                    <ModalFooter>
                        <Button onClick={onClose} p={5} m={15}>Cancel</Button>
                        <Button size="md" colorScheme="teal" p={5} m={15}>Send</Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    )
}

export default PichaHeader