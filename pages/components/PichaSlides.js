import { Badge, Box, Button, Image, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, useDisclosure, useInterval } from "@chakra-ui/react";
import moment from "moment";
import React, { useEffect, useState } from 'react';
import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";
import { BarChart } from 'react-d3-components';
import api from '../../api/api';
import styles from '../../styles/components/PichaSlider.module.css';
import PichaModal from "./modal/PichaModal";
let ml5 = null



const PichaSlider = () => {
    const [images, setImages] = useState([])
    const [predictions, setPredictions] = useState([])
    const { isOpen, onOpen, onClose } = useDisclosure()
    const [dataset, setDataset] = useState([{
        label: '',
        values: [{ x: '', y: 0 }, { x: '', y: 0 }, { x: '', y: 0 }]
    }])


    const classifyImg = async () => {

        const classifier = await ml5?.imageClassifier('MobileNet', modelLoaded);

        function modelLoaded() {

        }
        await classifier?.predict(document.getElementById('image'), function (err, results) {

            setPredictions(results)
        });
    }

    const graph = async () => {
        let trat = await images.map(item => {
            var d = {
                x: moment(item.date_taken).format("YYYY"),
                y: moment(item.date_taken).format("MM")
            }
            return d
        }
        )
        console.log(trat)

        var data = [{
            label: 'Date Record',
            values: trat
        }];

        setDataset(data)
        console.log(dataset)
    }
    const getImages = async () => {
        const result = await api.get('/')
        await setImages(result.data.items)

    }

    useInterval(() => {

        getImages()
        graph()
    }, 20000)


    useEffect(() => {
        ml5 = require('ml5')
        getImages()

    }, [])

    return (
        <>
            <Button size="lg" colorScheme="teal" mt={10} onClick={onOpen}>OPEN CHART</Button>
            <PichaModal closeOnOverlayClick={false} isOpen={isOpen} onClose={onClose} data={dataset} ></PichaModal>
            <div style={{ width: "70%", display: "flex", flexDirection: "row", alignContent: "center", justifyContent: "space-between" }}>
                <AliceCarousel autoPlay autoPlayInterval="10000" className={styles.container} onSlideChanged={classifyImg()}   >
                    {images.map(item => (
                        <Box borderWidth="1px" borderRadius="lg" overflow="hidden" height="70vh" marginTop="10" className={styles.boxContainer} >
                            <div className={styles.pred}>

                                {predictions?.map(item => (
                                    <p><strong>Possible image:</strong>{item.label} - <strong>Confidence:</strong>{parseFloat(item.confidence * 100).toFixed(2)}%</p>
                                ))}

                            </div>
                            <Box >
                                <Image src={item.media.m} objectFit="contain" id="image" height="40vh" width="50vw" crossorigin="anonymous" />

                            </Box>

                            <Box>
                                <Box d="flex" flexDir="row" flexWrap="wrap" alignItems="center" fontSize={10} >
                                    {
                                        item?.tags.split(" ").map(tg => (

                                            <Badge borderRadius="full" px="5" p="2" m="2" colorScheme="teal">
                                                {tg}
                                            </Badge>
                                        ))

                                    }
                                </Box>

                                <Box
                                    color="gray.500"
                                    fontWeight="semibold"
                                    letterSpacing="wide"
                                    fontSize="xs"
                                    textTransform="uppercase"


                                >
                                    {item.author}
                                </Box>

                                <Box
                                    mt="1"
                                    fontWeight="semibold"
                                    as="h4"
                                    lineHeight="tight"
                                    isTruncated
                                >
                                    {item.title}
                                    <p> Date:
                            {moment(item.published).format('DD-MM-YYYY')}</p>
                                </Box>

                            </Box>
                        </Box>
                    ))}
                </AliceCarousel>
            </div>
        </>
    )
}

export default PichaSlider