import { Box } from '@chakra-ui/layout'
import PichaChartOne from './components/PichaChartOne'
import PichaHeader from './components/PichaHeader'
import PichaSlider from './components/PichaSlides'
export default function Home() {


  return (
    <>

      <PichaHeader></PichaHeader>


      <Box w="100%" alignItems="center" d="flex" flexDir="column" justifyContent="center">
        <PichaSlider></PichaSlider>
      </Box>





    </>

  )
}
